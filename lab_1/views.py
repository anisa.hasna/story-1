from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Anisa Hasna Nabila' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2001, 12, 9) #TODO Implement this, format (Year, Month, Date)
npm = 1806146865 # TODO Implement this
kuliah = 'Fakultas Ilmu Komputer Universitas Indonesia'
hobi = 'menonton film, berolahraga, dan mengunjungi destinasi menarik di Indonesia'
deskripsi = 'saya termasuk orang yang senang bertemu dan berkenalan dengan \
            orang-orang baru. Saya juga senang belajar hal-hal baru namun \
            saya masih belum menemukan dimana minat dan kemampuan saya berada'
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'kuliah': kuliah,
                'hobi': hobi, 'deskripsi': deskripsi}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
